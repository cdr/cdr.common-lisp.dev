%%%% -*- Mode: LaTeX -*-

%%%% CL-PQs.tex --

\documentclass{article}


\usepackage{latexsym}
\usepackage{epsfig}
\usepackage{alltt}


\usepackage{titlesec}


\newcommand{\tm}{$^\mathsf{tm}$}
\newcommand{\cfr}{\emph{cfr.}}

\newcommand{\Lisp}{\textsf{Lisp}}
\newcommand{\CL}{\textsf{Common~Lisp}}

\newcommand{\Java}{\textsc{Java}}

\newcommand{\checkcite}[1]{{\textbf{[Missing Citation: #1]}}}
\newcommand{\checkref}[1]{{\textbf{[Missing Reference: #1]}}}

\newcommand{\missingpart}[1]{{\ }\vspace{2mm}\\
{\textbf{[Still Missing: #1]}}\\
\vspace{2mm}}

\newcommand{\marginnote}[1]{%
\marginpar{\raggedright \begin{small}\begin{em}#1
\end{em}\end{small}}}

%\addtolength{\textwidth}{2cm}


%%% CL 

\newcommand{\code}[1]{\texttt{#1}}

\newcommand{\term}[1]{\texttt{#1}}
\newcommand{\nonterm}[1]{\textit{$<$#1$>$}}


\newcommand{\kwd}[1]{\texttt{:#1}}
\newcommand{\variable}[1]{\textit{#1}}

\newcommand{\clglossary}[1]{\emph{#1}}

\newcommand{\CPL}{\subsubsection*{Class Precedence List:}}
\newcommand{\Syntax}{\subsubsection*{Syntax:}}
\newcommand{\ArgsValues}{\subsubsection*{Arguments and Values:}}
\newcommand{\Description}{\subsubsection*{Description:}}
\newcommand{\Examples}{\subsubsection*{Examples:}}
\newcommand{\AffectedBy}{\subsubsection*{Affected by:}}
\newcommand{\Exceptions}{\subsubsection*{Exceptional Situations:}}
\newcommand{\SeeAlso}{\subsubsection*{See Also:}}
\newcommand{\Notes}{\subsubsection*{Notes:}}


%%% Document specific macros.

\newcommand{\heap}{\texttt{heap}}
\newcommand{\heapfinger}{\texttt{heap-finger}}



\title{\LARGE \bfseries Priority Queues for \CL{}}

\author{Ingvar Mattsson\\\texttt{<ingvar@google.com>}\\[2mm]Marco Antoniotti\\\texttt{<marco.antoniotti@unimib.it>}}

\begin{document}

\maketitle

\paragraph{Keywords:} Heap, Priority Queue, \CL{}.

\section{Introduction}

This is a specification for the intruduction of a common API for
\emph{priority queues}, also called \emph{heaps}, in \CL{}.  The
specification tries to take into account the common elements present
in the several implementations available on the Internet, and to
ensure that the API is generic enough to allow for the seamless
inclusion of particular flavors of heaps.  An inspiration for this
specification API is \cite{CLRS}, especially w.r.t., the discussion
about \textsc{Heaps} and \textsc{Fibonacci Heaps}.


\subsection{Rationale}

There is no standard \emph{heap} (or \emph{priority queue}) implementation
in the Common Lisp standard. It is, however, a useful data structure.
The intention of this document is to provide a portable, flexible,
heap API that can be used on essentially all data where storing
according to a ranking criterion makes sense.

This API specification carefully does not discuss how it behaves in a
multi-processing environment.


\subsection{Guarantees}

\subsubsection{Time complexity}

The heap data structure gives you $O(1)$ peek at one extreme of the
heap. It also gives you $O(\lg n)$ addition and removal from the heap.

However, the $O(\lg n)$ insertion and removal relies on an $O(1)$
comparison operator. With having user-specified comparison (and key
extraction) operators, the best guarantee the reference implementation
can give is that insertion and removal is $O(C \lg n)$ for a
comparator complexity of $O(C)$.

\subsubsection{Multi-processing}

There are no explicit multi-processing or concurrency guarantees for
the generic heaps. However, implementors are encouraged to add
recursive locks to each heap object and lock/unlock these as
necessary.

\subsubsection{Side-effects}

Any code that modifies an object currently present in a heap is likely to
breach the heap invariant.  Doing that is highly discouraged. However,
modifying things within an object that does not, in any way,
contribute to the value used in comparisons may be safe.


\subsection{Design Choices}

There are a few design choices to be made when specifying an API for
\emph{heaps}.  The following is a list of foreseen issues and their
tratment.

\subsubsection{Heap Test must be a Total Order}

There is no way for a \CL{} implementation to check and ensure that
the function that becomes the \emph{heap test} (cfr., the constuctor
\code{make-heap}) is a \emph{total order} (modulo equality).
Providing a function that does not represent a total order has
\emph{undefined consequences}.


\subsubsection{Equal Keys}

The relative order to elements in a heap that admits \emph{equal
  keys} is \emph{implementation dependent} and should not be relied
upon.


\titleformat{\subsection}{%
  \titlerule
  \vspace{1mm}}{\large\bfseries\thesubsection}{1em}{\large\bfseries\sffamily}
  
 
\section{Heaps Dictionary}

 % \subsection{Package}
 % All symbols of a generic heap implementation should be accessible in
 % a package names such that :genheap is a designator for the
 % package.

 % This means that only one GENHEAP implementation can be loaded in any
 % given image, without having to do package renaming. However, it makes
 % it easier for developers to write code against the package or probe
 % for the package's existence using FIND-PACKAGE.

 % The functional interface presented is a minimum interface, it may be
 % that an implementor is willing to give guarantees for the
 % implementation that extend beyond this.

 % The exact underlying data structures are not specified. The reference
 % implementations use CLOS, generic functions and methods on these for
 % its implementation. One of them defines new methods as new generic
 % heap types are asked for, the other one doesn't.


\subsection{Class \heap{}}

\CPL{}

\heap{}, \ldots, \code{T}

\Description{}

Any implementation of this specification will provide a \emph{class}
named \heap{}.

\Notes{}

Each implementation is given the liberty to choose
whether to use a\\\code{structure-class} or a \code{standard-class} (or
another full-blown CLOS class)\marginnote{This implies that specialized
  heaps can only be derived via single inheritance.}.


\subsection{Generic Function \code{heap-p}}

\Syntax{}

\code{heap-p} \variable{object} $\Rightarrow$ \variable{generalized-boolean}

\ArgsValues{}

\begin{description}
\item[\variable{object} --] an \clglossary{object}.
\item[\variable{generalized-boolean} --] a \clglossary{generalized boolean}.
\end{description}

\Description{}

This function returns \code{NIL} when called on a non-heap
\variable{object} and a non-null value if presented with a heap
\variable{object}.


\subsection{Slot Readers \code{heap-size}, \code{heap-total-size},\\
  \code{heap-key-function}, \code{heap-test-function}}

\Syntax{}

\code{heap-size} \variable{heap} $\Rightarrow$ \variable{size}

\noindent
\code{heap-total-size} \variable{heap} $\Rightarrow$ \variable{total-size}

\noindent
\code{heap-key-function} \variable{heap} $\Rightarrow$
\variable{keyfun}

\noindent
\code{heap-test-function} \variable{heap} $\Rightarrow$
\variable{cmpfun}


\ArgsValues{}

\begin{description}
\item[\variable{heap} --] a \heap{}.
\item[\variable{heap-key-function} --] a \clglossary{function designator}.
\item[\variable{heap-test-function} --] a \clglossary{function designator}.
\item[\variable{size} --] a (positive) integer\marginnote{Maybe be more precise}.
\item[\variable{total-size} --] a (positive) integer\marginnote{Maybe be more precise}.
\end{description}

\Description{}

The \code{heap-size} and \code{heap-total-size} return the number of
elements in the \variable{heap}

The \code{heap-key-function} and \code{heap-test-function} accessors
return the \emph{test} function and the \emph{key} function used by
the \variable{heap} implementation to maintain the heap invariant.


\subsection{Type \heapfinger{}}

Many operations on heaps require to ``change'' something that is
located in a certain ``position'' in the underlying data structure.
To support these operations the specification requires implementations
to provide an opaque type named \heapfinger{}, i.e., to provide a way
to keep a ``finger'' on a certain position within the
heap\footnote{The term ``finger'' has been extensively used in the
  algorithms and data structure literature.}.

As an example, a traditional implementation of heaps based on arrays
could define \heapfinger{} as

\begin{alltt}
(deftype \heapfinger{} () 'fixnum)
\end{alltt}

\Notes{}

This specification does not prescribe anything in particular regarding
the behavior of \heapfinger{}s and the garbage collector.  An
implementation is free to add a \code{:weak} key to the \code{make-heap}
constructor (see below) and to return a \emph{weak} \heapfinger{},
that works well with the garbage collector.


\subsection{Function \code{heap-finger-p}}

\Syntax{}

\code{heap-finger-p} \variable{object} $\Rightarrow$
\variable{boolean}

\ArgsValues{}

\begin{description}
\item [\variable{object} --] an \clglossary{object}.
\item [\variable{boolean} --] a \code{boolean}.
\end{description}

\Description{}

Returns \code{T} if \variable{object} is a \heapfinger{}, \code{NIL}
otherwise.


\subsection{Condition \code{heap-error}}

\CPL{}

\code{heap-error}, \code{simple-error}, \ldots, \code{T}

\Description{}

The root of specialized errors raised by the heap operations; the heap
for which the error is being signaled can be initialized with the
keyword \code{:heap} and can be read by the accessor
\code{heap-error-heap}.  The default for the underlying slot is \code{NIL}.

\SeeAlso{}

\code{heap-error-heap}.

\subsection{Function \code{heap-error-heap}}

\Syntax{}

\code{heap-error-heap} \variable{heap-error} $\Rightarrow$ \variable{heap}

\ArgsValues{}

\begin{description}
\item[\variable{heap-error} --] a \code{heap-error}
\item[\variable{heap} --] a \heap{}.
\end{description}

\Description{}

Returns the \variable{heap} associated to the condition
\variable{heap-error} or \code{NIL} if the slot is uninitialized.


\subsection{Condition \code{empty-heap-error}}

\CPL{}

\code{empty-heap-error}, \code{heap-error}, \ldots, \code{T}

\Description{}

The condition that may be signaled when certain operations are
attempted on an empty heap.

\SeeAlso{}

\code{heap-error-heap}, \code{heap-error}.


\subsection{Condition \code{invalid-heap-finger-error}}

\CPL{}

\code{invalid-heap-finger-error}, \code{heap-error},
\code{cell-error}, \ldots, \code{T}

\Description{}

The condition that may be signaled when certain operations are
attempted on an \emph{invalid} ``position'' in a heap.  The offending
\emph{finger} must be passed at initialization time with the keyword
\kwd{name}.


\SeeAlso{}

\code{heap-error-heap}, \code{heap-error}, \heapfinger{}.

\Notes{}

\code{invalid-heap-finger-error} inherits from \code{cell-error},
hence, \code{cell-error-name} is used to get the offending \emph{finger}.


\subsection{Condition \code{invalid-key-error}}

\CPL{}

\code{invalid-key-error}, \code{heap-error}, \ldots, \code{T}

\Description{}

The condition that may be signaled when certain operations are
attempted with an \emph{invalid} ``key'' in a heap.  The offending key
is initialized using the \kwd{offender} keyword and can be retrieved
by the \code{invalid-key-error-offender} function.


\SeeAlso{}

\code{invalid-key-error-offender},
\code{heap-error-heap}, \code{heap-error}.



\subsection{Function \code{invalid-key-error-offender}}

\Syntax{}

\code{invalid-key-error-offender} \variable{i-k-e} $\Rightarrow$
\variable{key-object}

\ArgsValues{}

\begin{description}
  \item[\variable{i-k-e} --] a \code{invalid-key-error}.
  \item[\variable{key-object} --] a \clglossary{object}.
\end{description}

\Description{}

Given an instance of \code{invalid-key-error},
\code{invalid-key-error-offender} returns the offending
\variable{key-object} associated with \variable{i-k-e}.



\subsection{Function \code{make-heap}}

\Syntax{}

\code{make-heap} \code{\&key} \variable{test}  \variable{key}
\variable{initial-size} \variable{class} \variable{initial-contents}
\code{\&allow-other-keys}
$\Rightarrow$ \variable{heap}

\subsubsection*{Arguments and Values:}

\begin{description}
  \item[\variable{test} --] a \clglossary{function designator} for a binary
    function returning a \clglossary{generalized boolean}; default is
    \verb|<|.\marginnote{Make it \code{lt} from the Equality CDR?  :-)~:-)}
  \item[\variable{key} --] an accessor for an object; default is
    \code{identity}.
  \item[\variable{initial-size} --] a positive fixnum\marginnote{Be maybe more
      specific on the integer type?}; default is \code{16}.
  \item[\variable{class} --] a \clglossary{class designator}; the
    default is \heap{}.
  \item[\variable{heap} --] an instance of the \heap{} class or of any
    of its descendant classes.
\end{description}

\subsubsection*{Description:}

Returns a newly created \variable{heap}, using the specified \variable{test} as the heap
criterion, using \variable{key} to extract the values to be compared.


\subsection{Generic Function \code{empty-heap-p}}

\Syntax{}

\code{empty-heap-p} \variable{heap} $\Rightarrow$ \variable{boolean}


\ArgsValues{}

\begin{description}
\item [\variable{heap} --] a \heap{}.
\item [\variable{boolean} --] a \code{boolean}.
\end{description}


\Description{}

This function returns \code{T} when called on an empty heap, \code{NIL} otherwise.


\subsection{Generic Function \code{full-heap-p}}

\Syntax{}

\code{full-heap-p} \variable{heap} $\Rightarrow$ \variable{boolean}


\ArgsValues{}

\begin{description}
\item [\variable{heap} --] a \heap{}.
\item [\variable{boolean} --] a \code{boolean}.
\end{description}


\Description{}

This function returns \code{T} when no more values can be inserted in
the \variable{heap}, \code{NIL} otherwise.

Certain versions of heaps are only limited by the systems memory
limitations.  In these cases \code{full-heap-p} always returns
\code{NIL}.  Implementations are required to document these cases.


\subsection{Generic Function \code{insert}}

\Syntax{}

\code{insert} \variable{heap} \variable{value} $\Rightarrow$
\variable{value} \variable{finger}

\ArgsValues{}

\begin{description}
\item[\variable{heap} --] a \heap{}.
\item[\variable{value} --] an \clglossary{object}.
\item[\variable{finger} --] a \heapfinger{}.
\end{description}

\Description{}

Inserts a new \variable{value} into the \variable{heap}. The
\variable{value} inserted is returned alongside the ``location'',
pointed by \variable{finger} in which it was inserted.


\subsection{Generic Functions \code{extract}, \code{extract-from}}

\subsubsection*{Syntax:}

\code{extract} \variable{heap} \code{\&optional} \variable{default}
\variable{error-if-empty} $\Rightarrow$ \variable{value}\marginnote{This
  was \code{remove}.}

\noindent
\code{extract-from} \variable{heap} \variable{finger} \code{\&optional}
\variable{default} $\Rightarrow$ \variable{value}

\ArgsValues{} 

\begin{description}
\item[\variable{heap} --] a \heap{}.
\item[\variable{finger} --] a \heapfinger{}.
\item[\variable{default} --] an \clglossary{object}; default is \code{NIL}.
\item[\variable{error-if-empty} --] a \clglossary{generalized
    boolean}; default is \code{NIL}.
\item[\variable{value} --] an \clglossary{object}.
\end{description}

\Description{}

\code{extract} removes and returns the \variable{value} at the top of
the \variable{heap}, unless the \variable{heap} is empty. If the
\variable{heap} is empty and \variable{error-if-empty} is \code{NIL},
\variable{default} is returned; otherwise an \code{empty-heap-error}
error is signaled.

\code{extract-from} removes and returns the \variable{value} present
in the \variable{heap} in ``position'' \variable{finger}.   If the
\variable{finger} is invalid and \variable{error-if-empty} is \code{NIL},
\variable{default} is returned; otherwise an
\code{invalid-heap-finger-error} error is signaled.

\Exceptions{}

The errors \code{empty-heap-error} and
\code{invalid-heap-finger-error} are signaled in the case described above.


\subsection{Generic Function \code{peek}}

\Syntax{}

\code{peek} \variable{heap} \code{\&optional} \variable{default}
\variable{error-if-empty} $\Rightarrow$ \variable{value}

\ArgsValues{} 

\begin{description}
\item[\variable{heap} --] a \heap{}.
\item[\variable{default} --] an \clglossary{object}; default is \code{NIL}.
\item[\variable{error-if-empty} --] a \clglossary{generalized
    boolean}; default is \code{NIL}.
\item[\variable{value} --] an \clglossary{object}.
\end{description}

\Description{}

Returns the value at the top of the \variable{heap}, without modifying
the \variable{heap}.   If the \variable{heap} is empty and
\variable{error-if-empty} is \code{NIL}, \variable{default} is
returned; otherwise an error of type \code{empty-heap-error} is
signaled.

\SeeAlso{}

\code{empty-heap-error}


\subsection{Generic Functions \code{change-key},
  \code{decrease-key},\\\code{increase-key}}

\Syntax{}

\code{change-key} \variable{heap} \variable{new-key} \variable{finger}
$\Rightarrow$ \variable{heap} \variable{old-key} \variable{new-finger}

\noindent
\code{decrease-key} \variable{heap} \variable{new-key} \variable{finger}
$\Rightarrow$ \variable{heap} \variable{old-key} \variable{new-finger}

\noindent
\code{increase-key} \variable{heap} \variable{new-key} \variable{finger}
$\Rightarrow$ \variable{heap} \variable{old-key} \variable{new-finger}


\ArgsValues{} 

\begin{description}
\item[\variable{heap} --] a \heap{}.
\item[\variable{new-key} --] an \clglossary{object}.
\item[\variable{finger} --] a \heapfinger{}.
\item[\variable{old-key} --] an \clglossary{object}.
\item[\variable{new-finger} --] a \heapfinger{}.
\end{description}

\Description{}

\code{change-key} changes the key corresponding to the \variable{heap}
entry at position \variable{finger} with \variable{new-key}; the
\variable{heap} is restructured as a consequence.  The three values
returned are the restructured \variable{heap}, the key
(\variable{old-key}) used before the \code{change-key} had any effect
on the \variable{heap} structure, and the \variable{new-finger}
resulting after the changes effected by \code{change-key}.

The generic functions \code{decrease-key} and \code{increase-key},
check that \variable{new-key} is, respectively, ``smaller'' or
``greater'' than \variable{old-key} (the key associated to
\variable{finger}).  If the check succeeds, then the effect of the call
is that of calling \code{change-key}.\marginnote{So, do we or don't we
  call \code{change-key}?}  If the check fails than an error of type
\code{invalid-key-error} is
signaled.

\SeeAlso{}

\code{invalid-key-error}.


\Notes{}

It is assumed that all implementations will actually wrap the actual
heap internal data structure in a container shell of some kind.  I.e.,
the \variable{heap} is returned as such, with only the inside
structures changed as a consequence of \code{change-key}.



\subsection{Generic Function \code{fix-heap}}

\Syntax{}

\code{fix-heap} \variable{heap} \variable{finger}
$\Rightarrow$ \variable{heap} \variable{new-finger}

\ArgsValues{} 

\begin{description}
\item[\variable{heap} --] a \heap{}.
\item[\variable{finger} --] a \heapfinger{}.
\item[\variable{new-finger} --] a \heapfinger{}.
\end{description}

\Description{}

This function is used to \emph{fix} the heap invariant starting from a
given \variable{finger}.  This function should be used after changes
to an object stored in the \variable{heap} affecting the heap
invariant (cfr., \code{(setf~value-at)}).

\SeeAlso{}

\code{(setf~value-at)}.



\subsection{Generic Functions \code{key-at},
  \code{value-at},\\\code{content-at},  \code{content-at*}}

\Syntax{}

\code{key-at} \variable{heap} \variable{finger} $\Rightarrow$
\variable{key}

\noindent
\code{value-at} \variable{heap} \variable{finger} $\Rightarrow$
\variable{value}

\noindent
\code{(setf value-at)} \variable{value} \variable{heap} \variable{finger} $\Rightarrow$
\variable{value}

\noindent
\code{content-at} \variable{heap} \variable{finger} $\Rightarrow$
\variable{key}, \variable{value}

\noindent
\code{content-at*} \variable{heap} \variable{finger} $\Rightarrow$
\variable{content}

\ArgsValues{}

\begin{description}
\item[\variable{heap} --] a \heap{}.
\item[\variable{finger} --] a \heapfinger{}.
\item[\variable{key} --] an \clglossary{object}.
\item[\variable{old-key} --] an \clglossary{object}.
\item[\variable{value} --] an \clglossary{object}.
\item[\variable{content} --] a \clglossary{cons} of the form
  \code{(}\variable{key}~\code{.}~\variable{value}\code{)}.
\end{description}

\Description{}

As the names imply, \code{key-at} returns the \variable{key} that can
be found in the \variable{heap} in correspondence of the
\variable{finger}.

\noindent
\code{value-at} returns the \variable{value} that can
be found in the \variable{heap} in correspondence of the
\variable{finger}.  The \code{setf} form can be used to modify what is
associated to \variable{key} in correspondence of the
\variable{finger}.   No change in the underlying heap structure is
required.  Therefore, in order to ensure that the heap invariants are
maintained after a \code{(setf~value-at)} the user \emph{may} have to
call \code{fix} explicitely.

\noindent
\code{content-at} returns two values: the
\variable{key} and the \variable{value} that can
be found in the \variable{heap} in correspondence of the
\variable{finger}.  \code{content-at*} behaves like \code{content-at}
but it returns a dotted pair
\code{(}\variable{key}~\code{.}~\variable{value}\code{)}.


\SeeAlso{}

\code{fix-heap}.

\Notes{}

Problems with \code{(setf~content-at)} may arise when
\code{heap-key-function} is\\\code{identity} or conceivably similar
cases.  When this happens, then\\\code{(setf~content-at)} may violate
the heap invariant.


\subsection{Generic Functions \code{merge-heaps}, \code{nmerge-heaps}}

\Syntax{}

\code{merge-heaps} \variable{heap1} \variable{heap2} \code{\&key}
\code{\&allow-other-keys} $\Rightarrow$ \variable{new-heap}

\noindent
\code{nmerge-heaps} \variable{heap1} \variable{heap2} \code{\&key}
\code{\&allow-other-keys} $\Rightarrow$ \variable{new-heap}


\ArgsValues{}

\begin{description}
\item[\variable{heap1} --] a \heap{}.
\item[\variable{heap2} --] a \heap{}.
\item[\variable{new-heap} --] a \heap{}.
\end{description}


\Description{}

\code{merge-heaps} constructs a \variable{new-heap} that contains all the values of
\variable{heap1} and \variable{heap2}.  The \code{nmerge-heaps} may
destructively modify either \variable{heap1} or \variable{heap2} (or
both) and may return either in lieu of \variable{new-heap}.

\Notes{}

It is understood that the performance guarantees for this operation
depend on the underlying implementation.


\subsection{Generic Functions \code{heap-keys},
  \code{heap-values},\\\code{heap-contents}}

\Syntax{}

\code{heap-keys} \variable{heap} \code{\&optional}
(\variable{result-type} \code{'list}) $\Rightarrow$ \variable{result}

\noindent
\code{heap-values}  \variable{heap} \code{\&optional}
(\variable{result-type} \code{'list}) $\Rightarrow$ \variable{result}

\noindent
\code{heap-contents}  \variable{heap} \code{\&optional}
(\variable{result-type} \code{'list}) $\Rightarrow$ \variable{result}

\ArgsValues{}

\begin{description}
\item[\variable{heap} --] a \heap{}.
\item[\variable{result-type} --] a designator for a
  \clglossary{sequence} type.
\item[\variable{result}] -- a \code{sequence} of type \variable{result-type}
\end{description}

\Description{}

\code{heap-keys} returns a sequence of \variable{result-type}
containing the \emph{keys} in the \variable{heap}.  \code{heap-values} returns a sequence of \variable{result-type}
containing the \emph{values} in the \variable{heap}.  \code{heap-contents} returns a sequence of \variable{result-type}
containing pairs \code{(}\emph{key}~\code{.}~\emph{value}\code{)} in
the \variable{heap}; i.e., with the default \variable{result-type} of
\code{list}, \variable{result} is a \clglossary{association list}.

\Exceptions{}

A \code{type-error} is signaled if \variable{result} cannot be coerced
to a sequence of type \variable{result-type}.

\Notes{}

The content of \variable{result} is not affected by interleaving
\code{change-key}'s.  Users cannot make assumptions on the behavior.



 % \subsubsection{heapify}
 %   (heapify sequence \&key (test \#'\textless) (key \#'identity)) $\Rightarrow$ heap

 % Creates a heap populated by the sequence passed in, returning a heap
 % that should behave identically as one created by MAKE-HEAP, then
 % populated element by element.




 
 % \subsubsection{MACRO declare-heap}
 %   (declare-heap \&key (test \#'<) (key \#'identity))

 % This macro either updates internal data structures about a specific
 % type of test/key pair or not. It should be called before any
 % heap with that test/key pair is created.

 % The presence of this macro is to make heap-creation time more
 % predictable, allowing the implementation to do any heavy
 % class-creation up front, instead of at the time of the first heap
 % being created.


\begin{thebibliography}{9}

\bibitem{CLRS}
\textit{Introduction to Algorithms}, TH~Cormen, CE~Leiserson,
RL~Rivest, and C~Stein, 3$^\mathrm{rd}$ed., MIT Press and McGraw-Hill,
2009.

\bibitem{ANSIHyperSpec}
\textit{The \CL{} Hyperspec,}
published online at
\texttt{http://www.lisp.org/HyperSpec/FrontMatter/index.html}, 1994.

\end{thebibliography}


\appendix

\section{Copying and License}

This work may be distributed and/or modified under the conditions of
the \emph{LaTeX Project Public License} (LPPL), either version 1.3 of this license
or (at your option) any later version. The latest version of this
license is in \texttt{http://www.latex-project.org/lppl.txt} and version 1.3 or
later is part of all distributions of LaTeX version 2005/12/01 or
later.

\noindent
This work has the LPPL maintenance status `maintained'.

\noindent
The Current Maintainer of this work is Marco~Antoniotti\\\texttt{<marco.antoniotti@unimib.it>}.




\end{document}

%%%% end of file -- CL-PQs.tex --

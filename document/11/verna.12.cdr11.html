<html lang="en">
<head>
<title>Standard output streams default behavior in terminal sessions</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="description" content="A proposal for unifying the way Common Lisp standard output streams
behave initially in terminal-based sessions. This document appears in
the Common Document Repository as CDR 11.">
<meta name="generator" content="makeinfo 4.8">
<link title="Top" rel="top" href="#Top">
<link href="http://www.gnu.org/software/texinfo/" rel="generator-home" title="Texinfo Homepage">
<!--

     Copyright (C) 2012 Didier Verna

     Permission is granted to make and distribute verbatim copies of
     this manual provided the copyright notice and this permission
     notice are preserved on all copies.

     Permission is granted to copy and distribute modified versions of
     this manual under the conditions for verbatim copying, provided
     also that the section entitled ``Copying'' is included exactly as
     in the original.

     Permission is granted to copy and distribute translations of this
     manual into another language, under the above conditions for
     modified versions, except that this permission notice may be
     translated as well.
   -->
<meta http-equiv="Content-Style-Type" content="text/css">
<style type="text/css"><!--
  pre.display { font-family:inherit }
  pre.format  { font-family:inherit }
  pre.smalldisplay { font-family:inherit; font-size:smaller }
  pre.smallformat  { font-family:inherit; font-size:smaller }
  pre.smallexample { font-size:smaller }
  pre.smalllisp    { font-size:smaller }
  span.sc    { font-variant:small-caps }
  span.roman { font-family:serif; font-weight:normal; } 
  span.sansserif { font-family:sans-serif; font-weight:normal; } 
--></style>
</head>
<body>
<h1 class="settitle">Standard output streams default behavior in terminal sessions</h1>
<div class="node">
<p><hr>
<a name="Top"></a>
Next:&nbsp;<a rel="next" accesskey="n" href="#Copying">Copying</a>,
Previous:&nbsp;<a rel="previous" accesskey="p" href="#dir">(dir)</a>,
Up:&nbsp;<a rel="up" accesskey="u" href="#dir">(dir)</a>

</div>

<h2 class="unnumbered">Standard output streams default behavior in terminal sessions</h2>

<p>This document contains a proposal for unifying the way Common Lisp
standard output streams behave initially in terminal-based sessions. 
This document appears in the Common Document Repository as CDR 11.

<ul class="menu">
<li><a accesskey="1" href="#Copying">Copying</a>:      The LPPL license
<li><a accesskey="2" href="#Motivation">Motivation</a>:   The standard is underspecified
<li><a accesskey="3" href="#Analysis">Analysis</a>:     Possible choices
<li><a accesskey="4" href="#Proposal">Proposal</a>:     Deciding on the best one
</ul>

   <blockquote>
Copyright &copy; 2012 Didier Verna

        <p>Permission is granted to make and distribute verbatim copies of this
manual provided the copyright notice and this permission notice are
preserved on all copies.

        <p>Permission is granted to copy and distribute modified versions of this
manual under the conditions for verbatim copying, provided also that the
section entitled &ldquo;Copying&rdquo; is included exactly as in the original.

        <p>Permission is granted to copy and distribute translations of this manual
into another language, under the above conditions for modified versions,
except that this permission notice may be translated as well. 
</blockquote>

<!-- ==================================================================== -->
<!-- Copying -->
<!-- ==================================================================== -->
<div class="node">
<p><hr>
<a name="Copying"></a>
Next:&nbsp;<a rel="next" accesskey="n" href="#Motivation">Motivation</a>,
Previous:&nbsp;<a rel="previous" accesskey="p" href="#Top">Top</a>,
Up:&nbsp;<a rel="up" accesskey="u" href="#Top">Top</a>

</div>

<h2 class="unnumbered">Copying</h2>

<blockquote>
This work may be distributed and/or modified under the conditions of the
LaTeX Project Public License, either version 1.3 of this license or (at
your option) any later version. The latest version of this license is in
http://www.latex-project.org/lppl.txt and version 1.3 or later is part
of all distributions of LaTeX version 2005/12/01 or later.

        <p>This work has the LPPL maintenance status `maintained'.

        <p>The Current Maintainer of this work is Didier Verna. 
</blockquote>

<!-- ==================================================================== -->
<!-- Motivation -->
<!-- ==================================================================== -->
<div class="node">
<p><hr>
<a name="Motivation"></a>
Next:&nbsp;<a rel="next" accesskey="n" href="#Analysis">Analysis</a>,
Previous:&nbsp;<a rel="previous" accesskey="p" href="#Copying">Copying</a>,
Up:&nbsp;<a rel="up" accesskey="u" href="#Top">Top</a>

</div>

<h2 class="chapter">1 Motivation</h2>

<p>The Common Lisp standard mandates the existence of several streams such
as <code>*STANDARD-OUTPUT*</code>, <code>*ERROR-OUTPUT*</code> and <code>*QUERY-IO*</code>. The purpose of
these streams, however, is only informally described, leading to
implementation-specific behavior.

   <p>This can be problematic for Lisp sessions started from a terminal
(without a graphical user interface) and standalone command-line
executables. As illustrated in the next section, the current behavior of
some standard output streams, notably with respect to shell redirection
may not only be different across implementations, but also contrary to
the user's expectations.

   <p>The purpose of this document is hence to illustrate the problem and
suggest that all Common Lisp implementations agree on one particular
scheme (one actually already adopted by two of them).

<!-- ==================================================================== -->
<!-- Analysis -->
<!-- ==================================================================== -->
<div class="node">
<p><hr>
<a name="Analysis"></a>
Next:&nbsp;<a rel="next" accesskey="n" href="#Proposal">Proposal</a>,
Previous:&nbsp;<a rel="previous" accesskey="p" href="#Motivation">Motivation</a>,
Up:&nbsp;<a rel="up" accesskey="u" href="#Top">Top</a>

</div>

<h2 class="chapter">2 Analysis</h2>

<p>In order to analyze the effects of these underspecifications, some tests
were conducted on 8 Common Lisp implementations on May 28th 2012, with
the help of the code snippet depicted below.

   <p><table class="cartouche" summary="cartouche" border="1"><tr><td>
<pre class="verbatim">
(format *standard-output* "This goes to standard output.~%")
(format *error-output* "This goes to error output.~%")
(format *query-io* "This goes to query io.~%")
</pre>
</td></tr></table>

   <p>This code was stored in a test file, and loaded in three different
stream redirection contexts. Care was taken to avoid launching any
graphical user interface for implementations providing them. The
contexts were as follows (adapt the exact command-line settings to every
tested implementation):

   <p><table class="cartouche" summary="cartouche" border="1"><tr><td>
<pre class="verbatim">
cl --quit --load test.lisp
cl --quit --load test.lisp > log
cl --quit --load test.lisp > log 2>&amp;1
</pre>
</td></tr></table>

   <p>The results of these tests are depicted in the table below. They exhibit
3 different sets of behaviors.

   <p><table summary=""><tr align="left"><th valign="top">Compiler </th><th valign="top">Test case #1 </th><th valign="top">Test case #2 </th><th valign="top">Test case #3
<br></th></tr><tr align="left"><td valign="top">SBCL
</td><td valign="top"><tt>std-output -> tty</tt>
</td><td valign="top"><tt>std-output -> log</tt>
</td><td valign="top"><tt>std-output -> log</tt>
<br></td></tr><tr align="left"><td valign="top">CMU-CL
</td><td valign="top"><tt>err-output -> tty</tt>
</td><td valign="top"><tt>err-output -> tty</tt>
</td><td valign="top"><tt>err-output -> log</tt>
<br></td></tr><tr align="left"><td valign="top"></td><td valign="top"><tt>query-io   -> tty</tt>
</td><td valign="top"><tt>query-io   -> tty</tt>
</td><td valign="top"><tt>query-io   -> tty</tt>
<br></td></tr><tr align="left"><td valign="top"> <br></td></tr><tr align="left"><td valign="top">ECL
</td><td valign="top"><tt>std-output -> tty</tt>
</td><td valign="top"><tt>std-output -> log</tt>
</td><td valign="top"><tt>std-output -> log</tt>
<br></td></tr><tr align="left"><td valign="top"></td><td valign="top"><tt>err-output -> tty</tt>
</td><td valign="top"><tt>err-output -> tty</tt>
</td><td valign="top"><tt>err-output -> log</tt>
<br></td></tr><tr align="left"><td valign="top"></td><td valign="top"><tt>query-io   -> tty</tt>
</td><td valign="top"><tt>query-io   -> log</tt>
</td><td valign="top"><tt>query-io   -> log</tt>
<br></td></tr><tr align="left"><td valign="top"> <br></td></tr><tr align="left"><td valign="top">CLISP
</td><td valign="top"><tt>std-output -> tty</tt>
</td><td valign="top"><tt>std-output -> log</tt>
</td><td valign="top"><tt>std-output -> log</tt>
<br></td></tr><tr align="left"><td valign="top">CCL
</td><td valign="top"><tt>err-output -> tty</tt>
</td><td valign="top"><tt>err-output -> log</tt>
</td><td valign="top"><tt>err-output -> log</tt>
<br></td></tr><tr align="left"><td valign="top">ABCL
</td><td valign="top"><tt>query-io   -> tty</tt>
</td><td valign="top"><tt>query-io   -> log</tt>
</td><td valign="top"><tt>query-io   -> log</tt>
<br></td></tr><tr align="left"><td valign="top">LispWorks
<br></td></tr><tr align="left"><td valign="top">Allegro
   <br></td></tr></table>

   <p>We believe that the behavior of SBCL and CMU-CL is the most intuitive
one. Shell redirections of the system's <code>stdin</code> and <code>stdout</code>
are honored by <code>*STANDARD-OUTPUT*</code> and <code>*ERROR-OUTPUT*</code>, and <code>*QUERY-IO*</code>
always stays on the terminal.

   <p>The current behavior of SBCL and CMU-CL is informally described as
follows by Nikodemus Siivola:

   <blockquote>
There are streams <code>*STDIN*</code>, <code>*STDOUT*</code>, <code>*STDERR*</code> and <code>*TTY*</code>. If
<samp><span class="file">/dev/tty</span></samp> cannot be opened, <code>*TTY*</code> is simply
<tt>(make-two-way-stream *stdin* *stdout*)</tt>.

        <p><code>*STANDARD-INPUT*</code>, <code>*STANDARD-OUTPUT*</code> and <code>*ERROR-OUTPUT*</code> start out as
synonym streams for <code>*STDIN*</code>, <code>*STDOUT*</code> and <code>*STDERR*</code>. <code>*TERMINAL-IO*</code>,
<code>*QUERY-IO*</code> and <code>*DEBUG-IO*</code> start out as synonym streams for <code>*TTY*</code>. 
</blockquote>

   <p>ECL behaves almost the same. The difference is with <code>*QUERY-IO*</code> which
follows the behavior of <code>*STANDARD-OUTPUT*</code>. The Common Lisp standard
stipulates that this stream &ldquo;should be used when asking questions to
the user&rdquo;. Consequently, the current behavior of ECL is problematic
because in cases #2 and #3, the user would never see the questions
asked.

   <p>The remaining implementations suffer from the same problem as ECL with
respect to <code>*QUERY-IO*</code>. An additional problem lies in the fact that
<code>*ERROR-OUTPUT*</code> follows the behavior of <code>*STANDARD-OUTPUT*</code>, even in case
#2 where the system's <code>stderr</code> is not redirected. We think that
this behavior is counter-intuitive as well.

<!-- ==================================================================== -->
<!-- Proposal -->
<!-- ==================================================================== -->
<div class="node">
<p><hr>
<a name="Proposal"></a>
Previous:&nbsp;<a rel="previous" accesskey="p" href="#Analysis">Analysis</a>,
Up:&nbsp;<a rel="up" accesskey="u" href="#Top">Top</a>

</div>

<h2 class="chapter">3 Proposal</h2>

<p>In light of this analysis, we think that CMU-CL and SBCL offer the most
intuitive behavior, as it closely matches what one would expect from a
regular command-line application operating in a POSIX environment. 
Therefore, we suggest that all implementations conform to this behavior
when run in a non-graphical mode.

</body></html>


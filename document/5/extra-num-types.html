<!-- -*- Mode: HTML -*- -->

<!-- extra-num-types.tex --
Sub-interval types for Common Lisp.
-->

<html>
<head>
<title>Sub-interval Numerical Types for Common Lisp</title>
</head>

<body>

<h1>Sub-interval Numerical Types for Common Lisp</h1>

<p>
Marco Antoniotti<br />
<tt>mantoniotti</tt> at <tt>common-lisp.net</tt>
</p>

<h1>Abstract</h1>

<p>
  This document presents a new set of portable type specifiers that
  can be used to improve the &quot;precision&quot; of type
  declarations in numerical code.
</p>


<h1>Introduction</h1>

<p>When working on numerical algorithms it is sometimes useful to further
constrain the types of certain values to sub-intervals of the usual
types present in Common Lisp.  A typical example is that of indices
running over the dimensions of an array: such integers values should
not be negative.  While several Common Lisp implementations already
have certain special &quot;sub-interval&quot; type specifiers that can be used
in implementation dependent code, it seems natural and relatively
uncontroversial to propose a set of specialized types that codify
usual mathematical numerical sets and intervals.  This document puts
forward such a proposal.</p>

<p>The rest of this document is organized in two parts: a description
of the new types proposed,
<!-- a description of the (very simple) reference implementation -->
and a brief discussion pertaining to the rationale and the foreseen costs
of adoption by the various implementers.</p>


<h1>Description</h1>

<p>The extra types are presented in terms of the original Common Lisp type they
partition.  As it will appear in the following, most types are simply
partitions around the appropriate <em>zero</em>.</p>


<h2>Numerical Sub-interval Types</h2>

<p>There are several numerical types which represent traditional
mathematical sets in their representation-dependent implementations.
The numerical types are the following:
<ul>
<li> <code>negative-<i>T</i></code></li>
<li> <code>non-positive-<i>T</i></code></li>
<li> <code>non-negative-<i>T</i></code></li>
<li> <code>positive-<i>T</i></code></li>
<li> <code>array-index</code></li>
</ul>
where <i>T</i> is one of <code>fixnum</code>, <code>integer</code>,
<code>rational</code>, <code>ratio</code>, <code>real</code>,
<code>float</code>, <code>short-float</code>,
<code>single-float</code>, <code>double-float</code>,
<code>long-float</code>.  Each of these types is defined in a very
straightforward way.  The pseudo-code in the subsections hereafter
shows how each type can be defined.</p>


<h3><code>FIXNUM</code> Sub-interval Types</h3>

<p>The subtypes of type <code>fixnum</code> may be defined as follows.
  Note that <code>fixnum</code> does not allow for compound type
  specifiers.  Moreover, There is no <em>class</em> for
  <code>fixnum</code> in the ANSI specification , only a
  <em>type</em>.  This is a consequence of several factors and choices
  made in  standardization process.</p>

<pre>
   (deftype negative-fixnum ()
      `(integer ,most-negative-fixnum -1))

   (deftype non-positive-fixnum ()
      `(integer ,most-negative-fixnum 0))

   (deftype non-negative-fixnum ()
      `(integer 0 , most-positive-fixnum))

   (deftype positive-fixnum ()
      `(integer 1 ,most-positive-fixnum))
</pre>

<p>
The predicates following predicates are also defined in
the most straightforward way.
<ul>
<li> <code>negative-fixnum-p</code> </li>
<li> <code>non-positive-fixnum-p</code> </li>
<li> <code>non-negative-fixnum-p</code> </li>
<li> <code>positive-fixnum-p</code> </li>
</ul>
</p>




<h3><code>INTEGER</code> Sub-interval Types</h3>

<p>The subtypes of type <code>integer</code> may be defined as follows.</p>

<pre>
   (deftype negative-integer ()
      '(integer * -1))

   (deftype non-positive-integer ()
      '(integer * 0))

   (deftype non-negative-integer ()
      '(integer 0 *))

   (deftype positive-integer ()
      '(integer 1 *))
</pre>

<p>
The following predicates are also defined in
the most straightforward way.
<ul>
<li> <code>negative-integer-p</code> </li>
<li> <code>non-positive-integer-p</code> </li>
<li> <code>non-negative-integer-p</code> </li>
<li> <code>positive-integer-p</code> </li>
</ul>
</p>



<h3><code>RATIONAL</code> Sub-interval Types</h3>

<p>The subtypes of type <code>rational</code> may be defined as follows.</p>

<pre>
   (deftype negative-rational ()
      '(rational * (0)))

   (deftype non-positive-rational ()
      '(rational * 0))

   (deftype non-negative-rational ()
      '(rational 0 *))

   (deftype positive-rational ()
      '(rational (0) *))
</pre>
The following predicates are also defined in
the most straightforward way.
<ul>
<li> <code>negative-rational-p</code> </li>
<li> <code>non-positive-rational-p</code> </li>
<li> <code>non-negative-rational-p</code> </li>
<li> <code>positive-rational-p</code> </li>
</ul>



<h3><code>RATIO</code> Sub-interval Types</h3>

<p>The subtypes of type <code>ratio</code> may be defined as follows.  Note
that <code>ratio</code> does not allow for compound type specifiers.  Also,
there are other technical difficulties in this case if we wanted to be
<em>very</em> coherent with the background of the ANSI specification.
<code>ratio</code>s are defined exactly as the
<em>ratio of two non-zero integers, whose greatest common divisor is
  one and of which the denominator is greater than one</em>.  A
consequence of the definition is that
<code>(typep 42 'ratio)</code>&nbsp;&rArr;&nbsp;<code>NIL</code>, and, in
particular, <code>(typep 0
  'ratio)</code>&nbsp;&rArr;&nbsp;<code>NIL</code>.  This makes it
very difficult to use the type specifier machinery effectively, and we
must resort to the <code>satisfies</code> type specifier.  A possible
definition of the <code>ratio</code> sub-interval based on
<code>satisfies</code> needs therefore the definition of the
<code>ratiop</code> predicate (which is absent from the ANSI
specification) alongside the <code>ratio-plusp</code> and
<code>ratio-minusp</code> predicates.</p>

<pre>
   (defun ratiop (x)
      (and (rationalp x)
           (> (denominator x) 1)))

   (defun ratio-plusp (x)
      (and (ratiop x) (plusp x)))

   (defun ratio-minusp (x)
      (and (ratiop x) (minusp x)))
</pre>

<p>
These predicates could be implemented more efficiently by a given
implementation.</p>

<p>Now it is possible to define the <code>ratio</code> types.</p>

<pre>
   (deftype negative-ratio ()
      '(satisfies ratio-minusp))

   (deftype non-positive-ratio ()
      'negative-ratio)

   (deftype non-negative-ratio ()
      'positive-ratio)

   (deftype positive-ratio ()
      '(satisfies ratio-plusp))
</pre>

<p>
The following predicates are also defined in
the most straightforward way.
<ul>
<li> <code>negative-ratio-p</code> </li>
<li> <code>non-positive-ratio-p</code> </li>
<li> <code>non-negative-ratio-p</code> </li>
<li> <code>positive-ratio-p</code> </li>
</ul>


<h3><code>REAL</code> Sub-interval Types</h3>

<p>The subtypes of type <code>real</code> may be defined as follows.</p>

<pre>
   (deftype negative-real ()
      '(real * (0)))

   (deftype non-positive-real ()
      '(real * 0))

   (deftype non-negative-real ()
      '(real 0 *))

   (deftype positive-real ()
      '(real (0) *))
</pre>

<p>
The following predicates are also defined in
the most straightforward way.
<ul>
<li> <code>negative-real-p</code>
<li> <code>non-positive-real-p</code>
<li> <code>non-negative-real-p</code>
<li> <code>positive-real-p</code>
</ul>
</p>




<h3><code>FLOAT</code> Sub-interval Types</h3>

<p>The subtypes of the various <em>float</em> types may be defined as
follows.</p>

<pre>
   (deftype negative-<i>T</i> () '(<i>T</i> * (<i>zero</i>)))
   (deftype non-positive-<i>T</i> () '(<i>T</i> * <i>zero</i>))
   (deftype non-negative-<i>T</i> () '(<i>T</i> <i>zero</i> *))
   (deftype positive-<i>T</i> () '(<i>T</i> (<i>zero</i>) *))
</pre>

<p>
where <i>T</i> is one of <code>float</code>, <code>short-float</code>,
<code>single-float</code>, <code>double-float</code>, and
<code>long-float</code>.
Also, <i>zero</i> is written in the appropriate syntax;
i.e.,
<ul>
<li> <code>0.0E0</code> for <code>float</code> types </li>
<li> <code>0.0S0</code> for <code>short-float</code> types </li>
<li> <code>0.0F0</code> for <code>single-float</code> types </li>
<li> <code>0.0D0</code> for <code>double-float</code> types </li>
<li> <code>0.0L0</code> for <code>long-float</code> types </li>
</ul>
as per the ANSI specification.</p>

<p>The appropriate type predicates (whose names are not listed here) can
be implemented in the usual straightforward way.</p>


<h3><code>ARRAY-INDEX</code> Sub-interval Type</h3>

<p>The <code>array-index</code> type is obviously useful.  The type is
  already used in many implementations.  It is just not
  standardized.  The definition of <code>array-index</code> could be
  the following one.</p>

<pre>
   (deftype array-index ()
      `(integer 0 (,array-dimension-limit)))
</pre>

<p>The <code>array-index-p</code> predicate can thus be defined immediately.</p>


<h1>Discussion</h1>

<p>The proposed types are obviously just a convenience, yet they may be
used to improve the &quot;self-documenting&quot; nature of programs.  Some of
the definitions are directly useful.  Looping through an array can be
done in several ways, but often one just forgoes to write down the
proper index declaration or resorts to the practically omni-present
implementation-dependent equivalent of <code>array-index</code>.</p>

<p>The cost of adoption is very small for this facility.  Legacy code is
not affected and new code may - as stated - become more
self-documenting, and possibly more efficient.</p>

<!--
<p>Some of the names are long.  This is in the tradition of Common Lisp.
However, it would be possible to provide abbreviated versions by
shortening them using the following scheme
<ul>
<li> <code>negative</code> becomes <code>neg</code> </li>
<li> <code>non-negative</code> becomes <code>nonneg</code> </li>
<li> <code>positive</code> becomes <code>pos</code> </li>
<li> <code>non-positive</code> becomes <code>nonpos</code> </li>
</ul>
-->

<h1>References</h1>

<p>[1] K.&nbsp;M.&nbsp;Pitman
  <i><a href="http://www.lisp.org/HyperSpec/FrontMatter/index.html">The
      Common Lisp Hyperspec</a></i>, published online at
  <a href="http://www.lisp.org/HyperSpec/FrontMatter/index.html">http://www.lisp.org/HyperSpec/FrontMatter/index.html</a>,
  1994.</p>


</html>
<!-- end of file -- extra-num-types.tex -->

